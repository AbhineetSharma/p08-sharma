#Game README... Instruction to how to play...! #


### Project 08 Game ReadeMe ###



The game is developed in UNITY and build to a IOS Xcode Project.

The game is 3d pathfinding game with Virtual Joystick and flick Camera gestures.

In the game, the player navigate the ⚽️  though the open maze to get to a switch(neon blue),
while going through the Barriers(Yellow) and passing through winner box(neon red box).

### How do I get set up? ###

* Start the game
* Choose the color of the ball from 16 different color
* Select out off 2 levels to play
* Use the virtual joystick, flick camera, and boost button to find the way to win box(neon red), and before that go to the neon blue switch to remove the big obstacles(Red)
* Pass through walls(Yellow) with boost and balance the ⚾️ on the plane and finish the level.
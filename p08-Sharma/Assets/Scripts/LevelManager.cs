﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	private static LevelManager instance;
	public static LevelManager Instance{get{ return instance;}}
	// Use this for initialization
	public GameObject pauseMenu;

	private float startTime;
	public Transform respawnPoint;
	private GameObject player;

	private void Start(){
		instance = this;
		pauseMenu.SetActive (false);
		player = GameObject.FindGameObjectWithTag ("Player");
		player.transform.position = respawnPoint.position;
	}

	private void Update(){
		if (player.transform.position.y < - 10.0f) {
			player.transform.position = respawnPoint.position;
			player.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			player.GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;
		}
	}
	public void TogglePauseMenu(){
		pauseMenu.SetActive (!pauseMenu.activeSelf);
	}
		
	public void ToMenu(){
		SceneManager.LoadScene ("MainMenu");
	}
	public void Victory(){
		//float duration = Time.time - startTime;
		Debug.Log("level");
		SceneManager.LoadScene ("MainMenu");
	}
}

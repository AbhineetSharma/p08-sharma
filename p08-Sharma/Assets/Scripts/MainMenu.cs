﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LevelData{
	public LevelData(string levelName){
		string data = PlayerPrefs.GetString(levelName);
	}
}
public class MainMenu : MonoBehaviour
{
	
	private const float CAMERA_TRANSITION_SPEED = 3.0f;

	public GameObject levelButtonPrefeb;
	public GameObject levelButtonContainer;
	public GameObject shopButtonPrefeb;
	public GameObject shopButtonContainer;
	public Text currencyText;

	public Material playerMaterial;

	private Transform cameraTransform;
	private Transform cameraDesiredLookAt;
	private bool nextLevelLocked;
	private void Start ()
	{
		nextLevelLocked = true;
		ChangePlayerSkin (GameManager.Instance.currentSkinIndex);
		currencyText.text ="Color"; //string.Format ("{0} : {1}", "Currency", GameManager.Instance.currency.ToString ());
		cameraTransform = Camera.main.transform;

		Sprite[] thumbnails = Resources.LoadAll<Sprite> ("Levels");
		foreach (Sprite thumbnail in thumbnails) {
			GameObject container = Instantiate (levelButtonPrefeb) as GameObject;
			container.GetComponent<Image> ().sprite = thumbnail;
			container.transform.SetParent (levelButtonContainer.transform, false);

			string sceneName = thumbnail.name;
			new LevelData (sceneName);

			container.transform.GetChild (1).GetComponent<Image> ().enabled = !nextLevelLocked;
			//
			container.GetComponent<Button>().interactable= nextLevelLocked;
			nextLevelLocked = true;
			container.transform.GetChild(0).GetChild(0).GetComponent<Text>().text =  string.Format("Level {0}", sceneName.Split('_')[0].ToString());;
			container.GetComponent<Button> ().onClick.AddListener (() => LoadLevel (sceneName));
		}

		int textureIndex = 0;
		Sprite[] textures = Resources.LoadAll<Sprite> ("Player");
		foreach (Sprite texture in textures) {
			GameObject container = Instantiate (shopButtonPrefeb) as GameObject;
			container.GetComponent<Image> ().sprite = texture;
			container.transform.SetParent (shopButtonContainer.transform, false);

			//string sceneName = texture.name;
			int index = textureIndex;
			container.GetComponent<Button> ().onClick.AddListener (() => ChangePlayerSkin (index));
			//if ((GameManager.Instance.skinAvailability & 1 << index) == 1 << index) {
				container.transform.GetChild (0).gameObject.SetActive (false);
			//} 

			textureIndex++;
		}
	}

	private void Update ()
	{
		if (cameraDesiredLookAt != null) {
			cameraTransform.rotation = Quaternion.Slerp (cameraTransform.rotation, cameraDesiredLookAt.rotation, 3 * Time.deltaTime);
		}
	}

	private void LoadLevel (string scenename)
	{
		SceneManager.LoadScene (scenename);
		Debug.Log (scenename);

	}

	public void LookAtMenu (Transform menuTransform)
	{
		//Camera.main.transform.LookAt (menuTransform.position);
		cameraDesiredLookAt = menuTransform;
	}

	private void ChangePlayerSkin (int index)
	{

		if ((GameManager.Instance.skinAvailability & 1 << index) == 1 << index) {
			
		

			float x = (index % 4) * 0.25f;
			float y = ((int)index / 4) * .25f;

			if (y == 0.0f)
				y = 0.75f;
			else if (y == 0.25f)
				y = 0.5f;
			else if (y == 0.50f)
				y = 0.25f;
			else if (y == 0.75f)
				y = 0f;

			//playerMaterial.SetTextureOffset(
			playerMaterial.SetTextureOffset ("_MainTex", new Vector2 (x, y));

			GameManager.Instance.currentSkinIndex = index;
			GameManager.Instance.Save ();
		} else {
			int cost = 0;

			if (GameManager.Instance.currency >= cost) {
				GameManager.Instance.currency -= cost;
				GameManager.Instance.skinAvailability += 1 << index;
				GameManager.Instance.Save (); 
				//shopButtonContainer.transform.GetChild (index).GetChild (0).gameObject.SetActive (false);
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoBehaviour {

	public float moveSpeed = 5.0f;
	public float drag = 0.5f;
	public float terminalRotationSpeed = 25.0f;

	public VirtualJoystick moveJoystick;

	private float boostSpeed = 15.0f;
	private float boostCoolDown = 2.0f;
	private float lastBoost;

	private Rigidbody controller;
	private Transform camTransform;

	private bool nextLevelLocked = false;

	private void Start(){
		moveJoystick.InputDirection = Vector3.zero;
		lastBoost = Time.time - boostCoolDown;

		controller = GetComponent<Rigidbody> ();
		controller.maxAngularVelocity = terminalRotationSpeed;
		controller.drag = drag;

		camTransform = Camera.main.transform;

	}

	private void Update(){
		Vector3 dir = Vector3.zero;

		dir.x = Input.GetAxis ("Horizontal");
		dir.z = Input.GetAxis ("Vertical");

		if (dir.magnitude > 1) {
			dir.Normalize ();
  		}		
		if ( moveJoystick.InputDirection != Vector3.zero) {
			dir = moveJoystick.InputDirection;
		}
		Vector3 rotateDir = camTransform.TransformDirection (dir);
		rotateDir = new Vector3 (rotateDir.x, 0, rotateDir.z);
		rotateDir = rotateDir.normalized * dir.magnitude;

		controller.AddForce (rotateDir * moveSpeed);
	}

	public void Boost(){
		if (Time.time - lastBoost > boostCoolDown) {
			lastBoost = Time.time;
			controller.AddForce (controller.velocity.normalized * boostSpeed, ForceMode.VelocityChange);
		}
	}


}

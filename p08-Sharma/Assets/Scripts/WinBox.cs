﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinBox : MonoBehaviour {

	private void OnTriggerEnter(Collider col){
		//Debug.Log (col.tag);
		if (col.tag.Equals("Player")) {
			LevelManager.Instance.Victory ();
		}
	}
}
